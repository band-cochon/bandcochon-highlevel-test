package stepdefs;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.sql.*;

public class Account {
    private final String BaseUrl = "http://localhost:8000/mon-compte";
    private final FirefoxDriver driver;
    private String confirmUrl;
    private String username;
    private String email;

    public Account() {
        System.setProperty("webdriver.gecko.driver", "C:\\Program Files\\Mozilla Firefox\\geckodriver.exe");
        driver = new FirefoxDriver();
    }

    @Given("I'm on the main page")
    public void iMOnTheMainPage() {
        driver.get(BaseUrl);
    }

    @When("I click on the connection link")
    public void iClickOnTheLink() {
        WebElement connectionLink = driver.findElementById("id_connection");
        connectionLink.click();
    }

    @Then("I go to the account connection page")
    public void iGoToTheAccountConnectionPage() {
        String url = driver.getCurrentUrl();
        Assert.assertEquals(url, BaseUrl + "/connexion/");
    }

    @And("When I click on the create account button")
    public void whenIClickOnTheButton() {
        WebElement createButton = driver.findElementById("id_create_account");
        createButton.click();
    }

    @Then("I go the account creation page")
    public void iGoTheAccountCreationPage() {
        String url = driver.getCurrentUrl();
        Assert.assertEquals(url, BaseUrl + "/creation-compte/");

        driver.close();
    }

    @Given("I'm on the account creation page")
    public void iMOnTheAccountCreationPage() {
        driver.navigate().to(BaseUrl + "/creation-compte/");
    }

    @Then("The button Create Account is not clickable")
    public void theButtonCreateAccountIsNotClickable() {
        WebElement element = driver.findElementById("id_submit");
        Assert.assertNotNull(element);
        Assert.assertFalse(element.isEnabled());

        driver.close();
    }

    @Given("I'm on the account create page")
    public void iMOnTheAccountCreatePage() {
        driver.navigate().to(BaseUrl + "/creation-compte/");
    }

    @And("I fill user name field with {string}")
    public void iFillUserNameFieldWith(String username) {
        driver.findElementById("id_name").sendKeys(username);
    }

    @And("with the email with {string}")
    public void withTheEmailWith(String email) {
        driver.findElementById("id_mail").sendKeys(email);
    }

    @And("with the password is {string}")
    public void withThePasswordIs(String password) {
        driver.findElement(By.id("id_password")).sendKeys(password);
    }

    // Force to activate the button
    @And("I'm not a robot is clicked")
    public void iMNotARobotIsClicked() {
        ((JavascriptExecutor) driver).executeScript("document.getElementById('id_submit').removeAttribute('disabled')");
    }

    @Then("the strength indication is not empty")
    public void theStrengthIndicationIsNotEmpty() {
        WebElement element = driver
                .findElement(By.id("id_password_strength"))
                .findElement(By.tagName("span"));

        Assert.assertNotNull(element);
        String content = element.getText();
        Assert.assertNotEquals(content, "");
    }

    @And("the button create my account is clickable")
    public void theButtonCreateMyAccountIsClickable() {
        WebElement element = driver.findElement(By.id("id_submit"));
        Assert.assertTrue(element.isEnabled());
    }

    @When("I click on the create my account button")
    public void iClickOnTheCreateMyAccountButton() {
        driver.findElement(By.id("id_submit")).click();
    }

    @Then("the success panel is displayed")
    public void theSuccessPanelIsDisplayed() {
        driver.findElement(By.id("id_panel_success"));
    }

    @And("the user {string} is successfully created into the database as pending user")
    public void theUserIsSuccessfullyCreatedIntoTheDatabaseAsPendingUser(final String username) throws SQLException {
        Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/bandcochon?user=bandcochon&password=bandcochon");
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT COUNT(id) FROM auth_user WHERE username='"+ username + "'");
        resultSet.next();

        Assert.assertEquals(resultSet.getInt(1), 1);

        driver.close();
        connection.close();
    }

    @Given("The hash code from the database for the user {string}")
    public void theHashCodeFromTheDatabase(final String username) throws SQLException {
        Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/bandcochon?user=bandcochon&password=bandcochon");
        Statement hashStatement = connection.createStatement();
        ResultSet resultSet = hashStatement.executeQuery(
                "SELECT confirm_hash " +
                        "FROM bandcochon_utilisateur bu " +
                        "INNER JOIN auth_user au ON bu.user_id = au.id " +
                        "WHERE au.username = '"+ username + "'");
        resultSet.next();

        final String hash = resultSet.getString(1);
        confirmUrl = BaseUrl + "/creation-compte/confirm/" + hash + "/";
        connection.close();
    }

    @When("The user follow the link generated with this hash")
    public void theUserFollowTheLinkGeneratedWithThisHash() {
        driver.get(confirmUrl);
        driver.findElement(By.id("id-confirmation-success"));
    }

    @Then("the user {string} is registered")
    public void theUserIsRegistered(final String username) throws SQLException {
        Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/bandcochon?user=bandcochon&password=bandcochon");
        Statement hashStatement = connection.createStatement();
        ResultSet resultSet = hashStatement.executeQuery(
                "SELECT bu.registred " +
                        "FROM bandcochon_utilisateur bu " +
                        "INNER JOIN auth_user au ON bu.user_id = au.id " +
                        "WHERE au.username = '"+ username + "'");

        resultSet.next();
        final boolean registered = resultSet.getBoolean(1);
        Assert.assertTrue(registered);

        connection.close();
    }

    @And("he can login with email {string} and the password {string}")
    public void heCanLoginWithEmailAndThePassword(String email, String password) {
        driver.get(BaseUrl + "/connexion/");

        driver.findElement(By.name("email")).sendKeys(email);
        driver.findElement(By.name("password")).sendKeys(password);
        driver.findElement(By.id("id-connect")).click();

        driver.close();
    }

    @Given("The username {string} and the email {string}")
    public void theUsernameAndTheEmail(String username, String email) {
        this.username = username;
        this.email = email;
    }

    @When("I try to create an account")
    public void iTryToCreateAnAccount() {
        driver.get(BaseUrl + "/creation-compte/");
        driver.findElement(By.id("id_name")).sendKeys(this.username);
        driver.findElement(By.id("id_mail")).sendKeys(this.email);
    }

    @Then("A message display the user name already exists")
    public void aMessageDisplayTheUserNameAlreadyExists() {
        final WebElement nameElement = driver
                .findElement(By.id("id-error-name"))
                .findElement(By.tagName("em"));

        Assert.assertNotNull(nameElement);
        final String content = nameElement.getText();
        Assert.assertNotEquals(content, "");
    }

    @And("Another one tell the email too.")
    public void anotherOneTellTheEmailToo() {
        final WebElement mailElement = driver
                .findElement(By.id("id-error-mail"))
                .findElement(By.tagName("em"));

        Assert.assertNotNull(mailElement);
        final String content = mailElement.getText();
        Assert.assertNotEquals(content, "");

        driver.close();
    }
}
