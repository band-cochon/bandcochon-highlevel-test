import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import java.sql.*;

@RunWith(Cucumber.class)
@CucumberOptions(features = {"src/test/resources"})
public class AccountTest {
    @BeforeClass
    public static void beforeClass() throws SQLException {
        Connection connection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/bandcochon?user=bandcochon&password=bandcochon");
        Statement hashStatement = connection.createStatement();

        ResultSet resultSet = hashStatement.executeQuery("SELECT id FROM auth_user WHERE username='user_test'");

        if (resultSet.next()) {

            long id = resultSet.getLong(1);

            hashStatement.executeQuery(
                    "DELETE FROM bandcochon_utilisateur " +
                            "WHERE user_id = " + id);

            hashStatement.executeQuery(
                    "DELETE FROM auth_user " +
                            "WHERE id = " + id);
        }

        connection.close();
    }
}
