Feature: Create an account

  Scenario: Navigate to the account creation page
    Given I'm on the main page
    When I click on the connection link
    Then I go to the account connection page
    And When I click on the create account button
    Then I go the account creation page

  Scenario: Ensure the create account button is not enabled by default
    Given I'm on the account creation page
    Then The button Create Account is not clickable

  Scenario: Create a simple account
    Given I'm on the account create page
    And I fill user name field with "user_test"
    And with the email with "user_test@bandcochon.fr"
    And with the password is "TrucBidule!@"
    And I'm not a robot is clicked
    Then the strength indication is not empty
    And the button create my account is clickable
    When I click on the create my account button
    Then the success panel is displayed
    And the user "user_test" is successfully created into the database as pending user

  Scenario: Validating the user
    Given The hash code from the database for the user "user_test"
    When The user follow the link generated with this hash
    Then the user "user_test" is registered
    And he can login with email "user_test@bandcochon.fr" and the password "TrucBidule!@"

  Scenario: Create an user when the user already exists
    Given The username "user_test" and the email "user_test@bandcochon.fr"
    When I try to create an account
    Then A message display the user name already exists
    And Another one tell the email too.

